set terminal pngcairo font "arial,10" size 500,500
set output 'update.png'
set boxwidth 0.75
set style fill solid
set title "Tempo Execução Update"
set ylabel "Tempo (s)"
set xlabel "Quantidade"
plot "update.dat" using 2:xtic(1) with boxes