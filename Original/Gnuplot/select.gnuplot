set terminal pngcairo font "arial,10" size 500,500
set output 'select.png'
set boxwidth 0.75
set style fill solid
set title "Tempo Execução Select"
set ylabel "Tempo (s)"
set xlabel "Quantidade"
plot "select.dat" using 2:xtic(1) with boxes