set terminal pngcairo font "arial,10" size 500,500
set output 'insert.png'
set boxwidth 0.75
set style fill solid
set title "Tempo Execução Insert"
set ylabel "Tempo (s)"
set xlabel "Quantidade"
plot "insert.dat" using 2:xtic(1) with boxes