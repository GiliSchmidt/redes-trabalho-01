import sys
import socket
import ssl
import struct
import traceback
from threading import Lock, Thread
import WriteReadLocador as file
import locacao_pb2 as proto
import logging
logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(threadName)s:%(message)s')

from connection import (
    send_message, recv_message_by_type, SocketReadError
)

lock = Lock()

def main():
    host = '127.0.0.1'
    port = 999
    
    try:
        normal_sock = socket.socket()
        sock = ssl.wrap_socket(normal_sock, certfile='certificado.pem',keyfile='certificado.pem', ssl_version=ssl.PROTOCOL_TLSv1_2, server_side=True)
        
        #sock = socket.socket()
        sock.bind((host, port))
        sock.listen(10)
    
        logging.info ("Servidor iniciado na porta %s", str(port))

        file.init_database()
    
        while True:
            (conn, addr) = sock.accept()
            logging.info ("Cliente (%s, %s) conectado" % addr)
            Thread(target = get_message, args = (conn, addr) ).start()

        sock.close()
    except (KeyboardInterrupt, SystemExit):
        logging.info("Finalizando a execucacao ...")
        pass
    except:
        traceback.print_exc()

def get_message(sock, addr):
    try:
        recieve_message_from_client(sock, addr)
    except (SocketReadError):
         logging.critical ("Ocorreu um erro com o Cliente (%s, %s)" % addr)
    except:
        traceback.print_exc()   
    finally:
        sock.close()


def recieve_message_from_client(sock, addr):
    locacao = recv_message_by_type(sock, proto.Locacao)
    logging.info("Mensagem recebida do cliente (%s, %s)" % addr)

    global lock

    if(locacao.tipo_operacao == proto.Locacao.SELECT):
            msg = file.selectLocacao(locacao)[0] 
            send_message_to_client(sock, msg)
    elif(locacao.tipo_operacao == proto.Locacao.UPDATE):
        try:
            lock.acquire()
            file.updateLocacao(locacao)
        finally:
            lock.release()
    elif(locacao.tipo_operacao == proto.Locacao.INSERT):
        try:
            lock.acquire()
            file.addLocacao(locacao)
        finally:
            lock.release()
        
def send_message_to_client(sock, msg):
    print(msg.id_sala)
    send_message(sock, msg)

main()