------------------- SERVIDOR ------------------- 
Para utilizar o servidor basta executar o arquivo init_server.py
EX: python init_server.py


------------------- CLIENTE -------------------
Para utilizar o cliente basta executar o arquivo init_client.py
Devem ser informados como par�metro as informa��es a seguir descritas:


As opera��es disponiveis, como cliente, s�o:

I = INSERT
U = UPDATE
S = SELECT

Para utilizar as mesmas, siga o seguinte formato:

No INSERT, deve-se informar o nivel de acesso (se o locador pertence � unipampa ou n�o), sendo:
0 - Pertence � unipampa (onde tamb�m � necess�rio inserir o id unipampa)
1 - N�o pertence

INSERT

python init_client.py <ID SALA> I  <DESCRI��O> <DATA INICIO> <NOME LOCADOR> <ACESSO> <OPCIONAL CASO PERTEN�A A UNIPAMPA: ID UNIPAMPA>

EX:

python init_client.py "LAB 3" I  "Aula de redes" 20180422 "Diego" 0 idprofessorunipapa123123

python init_client.py "LAB 2" I  "Estudar" 20180423 "Luis" 1 




UPDATE

O UPDATE apenas atualiza a data final da loca��o, tendo o seguinte formato:

python init_client.py <ID SALA> U <DATAINICIO> <DATA FIM>

Exemplo:

python init_client.py "LAB 3" U 20180422 20180423




SELECT 

O SELECT retorna as informa��es de uma loca��o em determinado dia:

python init_client.py <ID SALA> S <DATA INICIO>

Exemplo:

python init_client.py "LAB 3" S 20180422

