# -*- coding: utf-8 -*-
import sys
import socket
import ssl
import struct
import traceback
import random
import string
import threading
import timeit
import logging
import locacao_pb2 as proto
logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(threadName)s:%(message)s')

from connection import (
    send_message, recv_message_by_type, SocketReadError
)


def connect_to_Server(message):
    host = '127.0.0.1'
    port = 999

    normal_sock = socket.socket()
    sock = ssl.wrap_socket(normal_sock, ssl_version=ssl.PROTOCOL_TLSv1_2, server_side=False)
        
    #sock = socket.socket()
    sock.connect((host, port))
    
    send_message(sock, message)

    sock.close()

def test_threads(method,i, t):
    threads = list()

    for n in range(0, t):
        t = threading.Thread(target = method, args= (i,))
        threads.append(t)
    
    for x in threads:
        x.start()
    for x in threads:
        x.join()

def test_insert(i):
    for n in range(0,i):
        locacao = proto.Locacao()
        locacao.id_sala = "Sala "+str(n)
        locacao.tipo_operacao = proto.Locacao.INSERT
        locacao.descricao = "descrição bem legal"
        locacao.data_inicio = 20180304
        locacao.locador.nome = "Nome de locador bem mais legal"
        locacao.locador.acesso = proto.Locacao.AUTORIZADO
        locacao.locador.id_unipampa= "ID do locador top"
        connect_to_Server(locacao)

def test_update(i):
    for n in range(0,i):
        locacao = proto.Locacao()
        locacao.id_sala = "Sala "+str(n)
        locacao.tipo_operacao = proto.Locacao.UPDATE
        locacao.data_fim = 20180305
        connect_to_Server(locacao)

def test_select(i):
    for n in range(0,i):
        locacao = proto.Locacao()
        locacao.id_sala = "Sala "+str(n)
        locacao.tipo_operacao = proto.Locacao.SELECT
        locacao.data_inicio = 20180304
        connect_to_Server(locacao)

def main():
    #numero de operacoes
    i = 5000
    #numero de threads
    t = 5


    start_time = timeit.default_timer()
    test_insert(i)
    #test_threads(test_insert,i, t)
    print("Tempo Insert: "+str(timeit.default_timer() - start_time) + " s")

    start_time = timeit.default_timer()
    test_update(i)
    #test_threads(test_update,i, t)
    print("Tempo Update: "+ str(timeit.default_timer() - start_time)+ " s")

    start_time = timeit.default_timer()
    test_select(i)
    #test_threads(test_select,i,t)
    print("Tempo Select: "+str(timeit.default_timer() - start_time)+"+ s")

main()
    