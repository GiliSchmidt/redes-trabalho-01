# -*- coding: utf-8 -*-
import locacao_pb2 as proto
import csv

locacoes = list()
database_path = ""
  

def init_database(database):
    with open(database, "r") as output:
        reader = csv.reader(output, lineterminator='\n', delimiter=';')
        global locacoes
        locacoes = list(reader)
        global database_path
        database_path = database



def addLocacao(locacao):
    id_sala = locacao.id_sala
    locador_nome = locacao.locador.nome
    locador_acesso = ""
    locador_id = ""

    if(locacao.locador.acesso == proto.Locacao.AUTORIZADO):
        locador_acesso = 0
        locador_id = locacao.locador.id_unipampa
    else:
        locador_acesso = 1
    
    data_inicio = locacao.data_inicio
    data_fim = locacao.data_fim
    descricao = locacao.descricao

    arraydados = [id_sala, locador_nome, locador_acesso, locador_id, data_inicio, data_fim, descricao]
    addSemQuebra(arraydados)


def addSemQuebra(arraydados):
    global locacoes
    locacoes.append(arraydados)
    with open(database_path, "a") as output:
        writer = csv.writer(output, lineterminator='\n', delimiter=';')
        writer.writerow(arraydados)


def selectLocacao(locacao):
    id_sala = str(locacao.id_sala)
    data_inicio = str(locacao.data_inicio)
    global locacoes

    for l in locacoes:
        if(str(l[0]) == id_sala and str(l[4]) == data_inicio):
            locacao.locador.nome = l[1]
            if(str(l[2]) == "0"):
                locacao.locador.acesso = proto.Locacao.AUTORIZADO
                locacao.locador.id_unipampa = l[3]
            else:
                locacao.locador.acesso = proto.Locacao.NAO_AUTORIZADO
            locacao.data_fim = int(l[5])
            locacao.descricao = l[6]
            return locacao, locacoes.index(l)

    locacao.id_sala = "Nada encontrado" 
    return locacao, 0

def updateLocacao(locacao):
    data_fim = locacao.data_fim
    loc, index = selectLocacao(locacao)

    if(locacao.id_sala == "Nada encontrado"):
        return
    else:
        loc_row = locacoes[index]
        loc_row[5] = data_fim
        with open(database_path, 'w') as output:
            writer = csv.writer(output, lineterminator='\n', delimiter=';')
            writer.writerows(locacoes)
