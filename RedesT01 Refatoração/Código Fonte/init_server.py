# -*- coding: utf-8 -*-

import sys
import socket
import ssl
import struct
import traceback
from threading import Lock, Thread
import WriteReadLocador as file
import locacao_pb2 as proto
import logging
logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(threadName)s:%(message)s')

from connection import (
    send_message, recv_message_by_type, SocketReadError
)

lock = Lock()

def main():
    if (len(sys.argv) < 2):
        print ("Parametros invalidos. Utilize ", sys.argv[0], " -help para ver os comandos disponiveis" )
        sys.exit()
    
    if(sys.argv[1] == "-help"):
        with open("help_server.txt", "r") as text_file:
            print(text_file.read())
        sys.exit()

    try:
        database = sys.argv[1]
        host = sys.argv[2]
        port = int(sys.argv[3])
    except:
        print ("Parametros invalidos. Utilize ", sys.argv[0], " -help para ver os comandos disponiveis" )
        sys.exit()

    
    try:
        normal_sock = socket.socket()
        sock = ssl.wrap_socket(normal_sock, certfile='file.pem',keyfile='file.key', ssl_version=ssl.PROTOCOL_TLSv1_2, server_side=True)
        
        sock.bind((host, port))
        sock.listen(10)
    
        logging.info ("Servidor iniciado na porta %s", str(port))

        file.init_database(database)
    
        while True:
            (conn, addr) = sock.accept()
            logging.info ("Cliente (%s, %s) conectado" % addr)
            Thread(target = get_message, args = (conn, addr) ).start()

        sock.close()
    except (KeyboardInterrupt, SystemExit):
        logging.info("Finalizando a execucacao ...")
        pass
    except:
        traceback.print_exc()

def get_message(sock, addr):
    try:
        recieve_message_from_client(sock, addr)
    except (SocketReadError):
         logging.critical ("Ocorreu um erro com o Cliente (%s, %s)" % addr)
    except:
        traceback.print_exc()   
    finally:
        sock.close()


def recieve_message_from_client(sock, addr):
    locacao = recv_message_by_type(sock, proto.Locacao)
    logging.info("Mensagem recebida do cliente (%s, %s)" % addr)

    global lock

    if(locacao.tipo_operacao == proto.Locacao.SELECT):
            msg = file.selectLocacao(locacao)[0] 
            send_message_to_client(sock, msg)
    elif(locacao.tipo_operacao == proto.Locacao.UPDATE):
        try:
            lock.acquire()
            file.updateLocacao(locacao)
        finally:
            lock.release()
    elif(locacao.tipo_operacao == proto.Locacao.INSERT):
        try:
            lock.acquire()
            file.addLocacao(locacao)
        finally:
            lock.release()
        
def send_message_to_client(sock, msg):
    send_message(sock, msg)

main()