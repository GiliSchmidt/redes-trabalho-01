# -*- coding: utf-8 -*-

import sys
import socket
import ssl
import struct
import traceback
import threading
import logging
import locacao_pb2 as proto
logging.basicConfig(level=logging.INFO, format='%(levelname)s:%(threadName)s:%(message)s')

from connection import (
    send_message, recv_message_by_type, SocketReadError
)

def main(): 
    if (len(sys.argv) < 2):
        print ("Parametros invalidos. Utilize ", sys.argv[0], " -help para ver os comandos disponiveis" )
        sys.exit()
    
    if(sys.argv[1] == "-help"):
        with open("help_client.txt", "r") as text_file:
            print(text_file.read())
        sys.exit()

    locacao = proto.Locacao()

    try:
        host = sys.argv[1]
        port = int(sys.argv[2])

        locacao.id_sala = sys.argv[3]

        if(sys.argv[4] == "U"):
            locacao.tipo_operacao = proto.Locacao.UPDATE
            locacao.data_inicio = int(sys.argv[5])
            locacao.data_fim = int(sys.argv[6])
        elif(sys.argv[4] == "S"):
            locacao.tipo_operacao = proto.Locacao.SELECT
            locacao.data_inicio = int(sys.argv[5])
        elif(sys.argv[4] == 'I'):
            locacao.tipo_operacao = proto.Locacao.INSERT
            locacao.descricao = sys.argv[5]
            locacao.data_inicio = int(sys.argv[6])

            locacao.locador.nome = sys.argv[7]
            if(sys.argv[8] == "0"):
                locacao.locador.acesso = proto.Locacao.AUTORIZADO
                locacao.locador.id_unipampa= sys.argv[9]
            else:
                locacao.locador.acesso = proto.Locacao.NAO_AUTORIZADO
    except:
        print("Parametros invalidos. Utilize ", sys.argv[0], " -help para ver os comandos disponiveis" )
        sys.exit()
        

    try:
        normal_sock = socket.socket()
        sock = ssl.wrap_socket(normal_sock, ssl_version=ssl.PROTOCOL_TLSv1_2, server_side=False)
        
        sock.connect((host, port))

        init_connection(sock, locacao)
    except (KeyboardInterrupt, SystemExit):
        logging.info("Finalizando a execucacao ...")
        pass
    except:
        traceback.print_exc()
    finally:
        sock.close()
   
def init_connection(sock, locacao):
    try:
        send_message_to_server(sock, locacao)

        if(locacao.tipo_operacao == proto.Locacao.SELECT):
            recieve_message_from_server(sock)
        else:
            print("Operação realizada com sucesso")
    except (SocketReadError):
        logging.critical ("Ocorreu um erro com o Servidor")
    except:
        traceback.print_exc()

def recieve_message_from_server(sock):
        message = recv_message_by_type(sock, proto.Locacao)

        if not message:
            print("Operação realizada com sucesso")
            return
        if(message.id_sala == "Nada encontrado"):
            print("Não foi encontrada nenhuma locação")
            return
    
        print("Resposta do servidor:")
        print("Sala: "+message.id_sala) 
        print("Locador: "+message.locador.nome)

        if(message.locador.acesso == proto.Locacao.AUTORIZADO):
            print("Pertence à UNIPAMPA: SIM")
        else:
            print("Pertence à UNIAPAMPA: Não")
        
        print("ID UNIPAMPA: "+message.locador.id_unipampa)
        print("Descrição: "+str(message.descricao))
        print("Data Inicio: "+str(message.data_inicio))
        print("Data Fim: "+ str(message.data_fim))
       


def send_message_to_server(sock, proto):
    send_message(sock, proto)

main()