# -*- coding: utf-8 -*-
import sys
import socket
import os
import timeit
import subprocess
from concurrent.futures import ThreadPoolExecutor
import subprocess

ip ="127.0.0.1"
port = 999

def teste_mult(users):
    #test insert
    pool = ThreadPoolExecutor(9)


    start_time = timeit.default_timer()
    for i in range(0,users):
        pool.submit(test_insert, i)
    pool.shutdown(wait=True)
    print("Tempo Insert Multithread: "+str(timeit.default_timer() - start_time) + " s")

    #test update
    start_time = timeit.default_timer()
    pool = ThreadPoolExecutor(9)
    for i in range(0,users):
        pool.submit(test_update, i)
    pool.shutdown(wait=True)
    print("Tempo Update Multithread: "+ str(timeit.default_timer() - start_time)+ " s")

    #test select
    start_time = timeit.default_timer()
    pool = ThreadPoolExecutor(9)
    for i in range(0,users):
        pool.submit(test_select, i)
    pool.shutdown(wait=True)
    print("Tempo Select Multithread: "+str(timeit.default_timer() - start_time)+"+ s")

def test_single(users):
    start_time = timeit.default_timer()
    for i in range(0,users):
       test_insert(i)
    print("Tempo Insert Single Thread: "+str(timeit.default_timer() - start_time) + " s")

    start_time = timeit.default_timer()
    for i in range(0,users):
        test_insert(i)
    print("Tempo Update Single Thread: "+ str(timeit.default_timer() - start_time)+ " s")

    start_time = timeit.default_timer()
    for i in range(0,users):
        test_insert(i)
    print("Tempo Select Single Thread: "+str(timeit.default_timer() - start_time)+"+ s")

def create_client(param):
    #os.system("python init_client.py "+param)
    subprocess.call(("python init_client.py "+param), shell=True, stdout=subprocess.PIPE)
    #subprocess.Popen("python init_client "+param, shell=True, stdin=subprocess.PIPE, stderr=subprocess.STDOUT, stdout=subprocess.PIPE, close_fds=True)


def test_insert(id):
    create_client("\""+str(ip)+"\" "+str(port)+ " \"LAB "+ str(id)+ "\" I \"Aula de redes\" 20180422 \"Diego\" 0 \"idprofessorunipapa123123\"")

def test_update(id):
    create_client("\""+str(ip)+"\" "+str(port)+ " \"LAB "+ str(id)+ "\" U 20180422 20180423")

def test_select(id):
    create_client("\""+str(ip)+"\" "+str(port)+ " \"LAB "+ str(id)+ "\" S 20180422")

def main():
    global ip
    ip = sys.argv[1]
    global port
    port = sys.argv[2]

    test_single(int(sys.argv[3]))
    teste_mult(int(sys.argv[3]))


main()






    
    
