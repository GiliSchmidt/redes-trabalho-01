----------------------------------------------------------------------------------------------------------------------------------------------------
INSERT

No INSERT, deve-se informar o nivel de acesso (se o locador pertence à unipampa ou não), sendo:
0 - Pertence à unipampa (onde também é necessário inserir o id unipampa)
1 - Não pertence

python init_client.py <IP SERVIDOR> <PORTA> <ID SALA> I  <DESCRIÇÃO> <DATA INICIO> <NOME LOCADOR> <ACESSO> <OPCIONAL CASO PERTENÇA A UNIPAMPA: ID UNIPAMPA>

Exemplo:

python init_client.py "192.168.1.1" 111 "LAB 3" I  "Aula de redes" 20180422 "Diego" 0 idprofessorunipapa123123
python init_client.py "192.168.1.1" 111 "LAB 2" I  "Estudar" 20180423 "Luis" 1 

----------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE

O UPDATE apenas atualiza a data final da locação, tendo o seguinte formato:

python init_client.py <IP SERVIDOR> <PORTA> <ID SALA> U <DATAINICIO> <DATA FIM>

Exemplo:

python init_client.py "192.168.1.1" 111 "LAB 3" U 20180422 20180423

----------------------------------------------------------------------------------------------------------------------------------------------------
SELECT 

O SELECT retorna as informações de uma locação em determinado dia:

python init_client.py <IP SERVIDOR> <PORTA> <ID SALA> S <DATA INICIO>

Exemplo:

python init_client.py "192.168.1.1" 111 "LAB 3" S 20180422
----------------------------------------------------------------------------------------------------------------------------------------------------